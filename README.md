# C_Practice 

## List of Files:

### FreeRTOS_Basics

The file shows an initial step into learning FreeRTOS expanding on Real-Time systems knowledge.
A simple program designed to concurrently calculate the value of pi in 2 tasks and print them separately.
The program also contains a watchdog timer and a task to suspend and calculation task to test the watchdog.
